package bongotest.test_1;
import java.util.Arrays;

/*
* AndroidBongoTestQuestion 01
* Code of: Write a function that detects if two strings are anagram e.g. ‘bleat’ and ‘table’ are
anagrams but ‘eat’ and ‘tar’ are not.
* */
public class Anagram {

    public static void main(String[] args) {

        String val1 = "bleat";
        String val2 = "table";

        System.out.print( "\'"
                + val1 + " and \'"
                + val2 +"\' "
                + (isAnagram(val1, val2)? "are Anagram":"aren't Anagram"));


    }


    public static boolean isAnagram(String val1, String val2) {

        // removing all whitespace
        val1 = val1.replaceAll("\\s", "");
        val2 = val2.replaceAll("\\s", "");

//        compare length of both values
        if (val1.length() != val2.length()) {
            // length of both are not equal, they are not anagram.
            return false;
        } else {


            // convert string values to char arrays
            char[] val1Array = val1.toLowerCase().toCharArray();
            char[] val2Array = val2.toLowerCase().toCharArray();

            // sort both char arrays
            Arrays.sort(val1Array);
            Arrays.sort(val2Array);

            return Arrays.equals(val1Array, val2Array);

        }


    }


}
