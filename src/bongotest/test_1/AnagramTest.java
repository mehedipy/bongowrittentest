package bongotest.test_1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/*
* AndroidBongoTestQuestion 01
* Unit Test of: Write a function that detects if two strings are anagram e.g. ‘bleat’ and ‘table’ are
anagrams but ‘eat’ and ‘tar’ are not.
* */

public class AnagramTest {
    private String[][] testValues = {{"eat", "‘tar"}, {"bleat", "table"}};



    @Test
    void test1() {

        // expected : false as they are anagram.
        boolean expected = false;

        // values are: "eat", "‘tar"
        boolean actual = Anagram.isAnagram(testValues[0][0], testValues[0][1]);
        // asserting
        assertEquals(expected, actual);

        System.out.print( "\'"
                + testValues[0][0] + " and \'"
                + testValues[0][1] +"\' "
                + (actual? "are Anagram":"aren't Anagram"));


    }

    @Test
    void test2() {

        // expected : false as they are anagram.
        boolean expected = true;

        // values are: "bleat", "‘table"
        boolean actual = Anagram.isAnagram(testValues[1][0], testValues[1][1]);
        // asserting
        assertEquals(expected, actual);
        System.out.print( "\'"
                + testValues[1][0] + " and \'"
                + testValues[1][1] +"\' "
                + (actual? "are Anagram":"aren't Anagram"));
    }
}
