package bongotest.test_2.solution02a;

import bongotest.test_2.Vehicle;

public class FactoryPatternDemo {

    public static void main(String[] args) {

        Vehicle car = VehicleFactory.getVehicle("car");
        Vehicle plane = VehicleFactory.getVehicle("plane");

        System.out.println("Number of Car Passengers: "+ car.set_num_of_passengers());
        System.out.println("Number of plane Passengers: "+ plane.set_num_of_passengers());
    }
}
