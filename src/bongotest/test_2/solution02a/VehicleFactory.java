package bongotest.test_2.solution02a;

import bongotest.test_2.Vehicle;

public class VehicleFactory {

    public static Vehicle getVehicle(String v){
        if (v == null){
            return null;
        }
        if (v.equalsIgnoreCase("CAR")){
            return new Car();

        }
        else if(v.equalsIgnoreCase("PLANE")){
            return new Plane();
        }
        else{
            return null;
        }

    }

}
