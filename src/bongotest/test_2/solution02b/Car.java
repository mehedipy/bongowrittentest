package bongotest.test_2.solution02b;

import bongotest.test_2.Vehicle;

public class Car implements Vehicle {
    @Override
    public int set_num_of_wheels() {
        System.out.println("Car have 4 wheels");
        return 4;
    }

    @Override
    public int set_num_of_passengers() {
        System.out.println("Car have 4 passengers");

        return 4;
    }

    @Override
    public boolean has_gas() {
        System.out.println("Car have gas");
        return true;
    }
}
