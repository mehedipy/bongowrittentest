package bongotest.test_2.solution02b;

public class FacadePatternDemo {
    public static void main(String[] args) {
        VehicleMaker vehicleMaker = new VehicleMaker();

        vehicleMaker.showCarProperties();
        vehicleMaker.showPlaneProperties();
    }
}
