package bongotest.test_2.solution02b;

import bongotest.test_2.Vehicle;

public class Plane implements Vehicle {
    @Override
    public int set_num_of_wheels() {
        System.out.println("Plane have 3 wheels");
        return 3;
    }

    @Override
    public int set_num_of_passengers() {
        System.out.println("Plane have 150 passengers");
        return 150;
    }

    @Override
    public boolean has_gas() {
        System.out.println("Plane have gas");
        return true;
    }
}
