package bongotest.test_2.solution02b;

import bongotest.test_2.Vehicle;

public class VehicleMaker {
    private Vehicle car;
    private Vehicle plane;

    public VehicleMaker() {
        car = new Car();
        plane = new Plane();

    }

    public void showCarProperties(){
        car.set_num_of_wheels();
        car.set_num_of_passengers();
        car.has_gas();
    }

    public void showPlaneProperties(){
        plane.set_num_of_wheels();
        plane.set_num_of_passengers();
        plane.has_gas();
    }
}
